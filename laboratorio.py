#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import random

# menu para selcecionas las acciones
def panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcetaje_ruedas, pits):
    # el global tenia un error y no supe como solucionarlo excepto con el global
    global porcentaje_ruedas
    print("c.- Caracteristicas de vehiculo")
    print("e.- Encender / Apagar")
    print("w.- si quiere avanzar")
    a = input("Ingrese lo que desea hacer: ")
    print("\n")
    if a == "c":
        auto.caracteristicas(estanque, ruedas, motor, estado, porcentaje_ruedas)
        print("\n")
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)
    # si el auto esta apagado estado igual a 0 y si se prende estado igual a 1
    if a == "e" and estado == 0:
        print("Auto encendido")
        estanque = estanque - (estanque / 100)
        print("\n")
        estado += 1
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)
    elif a == "e" and estado == 1:
        print("Aunto apagado")
        estado = 0
        print("\n")
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)
    if a == "w" and estado == 0:
        print("El auto esta apagado debe encenderlo para que avance")
        print("\n")
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)
    # Genera el movimiento segun las catacteriaticas del motor
    if a == "w" and estado == 1 and motor == 1.2:
        porcentaje_ruedas = auto.gasto_ruedas(porcentaje_ruedas, pits)
        estanque = auto.avanzar_12(tiempo, velocimetro, estanque, porcentaje_ruedas)
        print("\n")
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)
    if a == "w" and estado == 1 and motor == 1.6:
        porcentaje_ruedas = auto.gasto_ruedas(porcentaje_ruedas, pits)
        estanque = auto.avanzar_16(tiempo, velocimetro, estanque, porcentaje_ruedas)
        print("\n")
        panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)

# a los pits si las ruedas estan en mas estado las rapara
class Pits():
    def guido(self, porcentaje_ruedas):
        print("Porcentaje de las ruedas es: ", porcentaje_ruedas, "%, es necesario cambiarlar")
        print("A los pits")
        print("Se le han cambiado las ruedas")
        porcentaje_ruedas = 100

# Genera el motor aleatoria mente
class Motor():
    def version_motor(self, cilindrada):
        self.cilindrada = cilindrada

    def get_cilindrada(self, cilindrada):
        cilindrada = random.choice((1.2, 1.6))
        return cilindrada

# auto
class Auto():
    # imprime las caracteristicas del auto
    def caracteristicas(self, estanque, ruedas, motor, estado, porcenjate_ruedas):
        print("DATOS DE VEHICULO")
        print("Motor de: ", motor, )
        print("Ruedas: ", ruedas)
        print("Estado de las ruedas es: ", porcentaje_ruedas, "%")
        print("Estanque: ", estanque, "Litros")
        if estado == 0:
            print("El auto esta apagado")
        if estado == 1:
            print(("El auto esta encendido"))

    # Calcula el gasto de la ruedas y si estan muy gastada llama a reparacion
    def gasto_ruedas(self, porcentaje_ruedas, pits):
        g = porcentaje_ruedas - random.randint(1, 11)
        if g < 10:
            pits.guido(porcentaje_ruedas)
        return g

    # Avanzar el motor 1.2 si se queda sin galosina se detiene el programa
    def avanzar_12(self, tiempo, velocimetro, estanque, porcentaje_ruedas):
        tiempo = random.randrange(1, 11)
        d = velocimetro * tiempo
        # Se calcula lo maximo que puede andar por si se pasa de esa distancia
        max_d = 20 * estanque
        if d > max_d:
            # Si se pasa de la distancia calcula el tiempo
            r = d - max_d
            tiempo2 = r / velocimetro
            tiempo = tiempo - tiempo2
            print("El auto recorrio", max_d, "Km en ", tiempo, "segundos a una velocidad de: ", velocimetro, "Km")
            print("Estado de las ruedas es: ", porcentaje_ruedas, "%")
            print(("y se quedo sin gasolina"))
            exit()
        else:
            estanque = estanque - (d / 20)
            print("El auto avanzo ", d, "km en ", tiempo, "segundos a una velocidad de: ", velocimetro, "Km")
            print("gasto ", int(d / 20), "litros")
            print("Estado de las ruedas es: ", porcentaje_ruedas, "%")
            print("Le quedan ", estanque, "litros")
        return estanque
    # Avanza el motor 1.6
    def avanzar_16(self, tiempo, velocimetro, estanque, porcentaje_ruedas):
        tiempo = random.randrange(1, 11)
        d = velocimetro * tiempo
        max_d = 14 * estanque
        # se calcula la distancia maxima por si recorre mxs de que lo puede
        if d > max_d:
            r = d - max_d
            tiempo2 = r / velocimetro
            tiempo = tiempo - tiempo2
            print("El auto recorrio", max_d, "Km en ", tiempo, "segundos a una velocidad de: ", velocimetro, "Km")
            print("Estado de las ruedas es: ", porcentaje_ruedas, "%")
            print(("y se quedo sin gasolina"))
            exit()
        else:
            # si no ne gasta toda la gasosina retorna el estanque con lo que quedo de gasolinaq
            estanque = estanque - (d / 14)
            print("El auto avanzo ", d, "km en ", tiempo, "segundos a una velocidad de: ", velocimetro, "Km")
            print("estado de las ruedas es: ", porcentaje_ruedas, "%")
            print("gasto ", int(d / 14), "litros")
            print("Le quedan ", estanque, "litros")
        return estanque

# Main
if __name__ == '__main__':
    estado = 0
    estanque = 32
    ruedas = 4
    estado_ruedas = 0
    velocimetro = 120
    cilindrada = 0
    tiempo = 0
    porcentaje_ruedas = 100
    pits = Pits()
    m_version = Motor()
    motor = m_version.get_cilindrada(cilindrada)
    auto = Auto()
    panel_control(estado, estanque, auto, ruedas, motor, tiempo, velocimetro, porcentaje_ruedas, pits)